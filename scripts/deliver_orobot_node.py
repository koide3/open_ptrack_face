#!/usr/bin/python
import re
import cv2
import math
import numpy

import rospy
import sound_play
import actionlib
from std_msgs.msg import *
from sensor_msgs.msg import *
from geometry_msgs.msg import *
from opt_msgs.msg import *
from open_ptrack_face.msg import *
from tf2_msgs.msg import *
from move_base_msgs.msg import *


class DeliverOrobotNode:
	def __init__(self):
		self.soundclient = sound_play.libsoundplay.SoundClient()
		rospy.sleep(1)

		self.names = {}
		self.target_names = ['kenji', 'mattia', 'yongheng']
		print 'target:', self.target_names

		self.move_base_action = actionlib.SimpleActionClient('move_base', MoveBaseAction)
		while not self.move_base_action.wait_for_server(rospy.Duration(1)):
			print 'waiting for the action server'

		self.names_sub = rospy.Subscriber('/face_recognition/people_names', NameArray, self.names_callback, queue_size=1, buff_size=2**10)
		self.tracks_sub = rospy.Subscriber('/face_recognition/tracks', TrackArray, self.tracks_callback, queue_size=1, buff_size=2**10)
		self.poses_sub = rospy.Subscriber('/recognizer/poses', PoseRecognitionArray, self.poses_callback, queue_size=1, buff_size=2**10)

		self.waiting_to_take = False

		self.say('I bring cups to ' + ' and '.join(self.target_names), rospy.Duration(2.0))
		print 'ready'

	def say(self, text, sleep_duration):
		print 'say "%s"' % text
		self.soundclient.say(text, 'voice_kal_diphone', 1.0)
		rospy.sleep(sleep_duration)

	def tracks_callback(self, tracks_msg):
		if self.waiting_to_take:
			return

		for track in tracks_msg.tracks:
			face_id = track.id

			if face_id in self.names and self.names[face_id].lower() in self.target_names:
				person_name = self.names[face_id].lower()
				pos = (track.x, track.y, 0.0)
				print 'target found!!', pos
				self.say('I found ' + person_name, 2.0)
				self.move_to(pos)
				self.say('Ciao ' + person_name, 3.0)
				self.say('Take the cup', 2.0)
				self.target_names.remove(person_name)
				self.waiting_to_take = True

		if not self.waiting_to_take and len(self.target_names) == 0:
			self.say('I delivered all the cups.', rospy.Duration(2.0))
			self.say('Bye bye. I go home!', rospy.Duration(2.0))
			destination = (-0.664, -3.176, 0.0)
			self.move_to(destination)

			rospy.signal_shutdown('task accomplished')

	def names_callback(self, names_msg):
		names = {}
		for i in range(len(names_msg.ids)):
			names[names_msg.ids[i]] = names_msg.names[i]
		self.names = names

	def poses_callback(self, poses_msg):
		if not self.waiting_to_take:
			return

		for pose in poses_msg.poses:
			posename = pose.best_prediction_result.arms_pose_name
			if 'pointing' in posename:
				self.waiting_to_take = False
				self.say('Go to the next.', rospy.Duration(4.0))

	def move_to(self, pos):
		print 'move to', pos
		goal = MoveBaseGoal()
		goal_pose = goal.target_pose
		goal_pose.header.frame_id = '/world'
		goal_pose.header.stamp = rospy.Time.now()
		goal_pose.pose.position.x = pos[0]
		goal_pose.pose.position.y = pos[1]
		goal_pose.pose.position.z = 0.0

		goal_pose.pose.orientation.x = 0.0
		goal_pose.pose.orientation.y = 0.0
		goal_pose.pose.orientation.z = 0.0
		goal_pose.pose.orientation.w = 1.0

		self.move_base_action.send_goal_and_wait(goal)
		print 'done'


def main():
	print '--- deliver_orobot_node ---'
	rospy.init_node('deliver_orobot_node')
	node = DeliverOrobotNode()
	rospy.spin()

if __name__ == '__main__':
	main()
